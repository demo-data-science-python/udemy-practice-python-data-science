# Practice Python Data Science

ref : [【世界で5万人が受講】実践 Python データサイエンス \| Udemy](https://www.udemy.com/python-jp/learn/v4/overview)

## Environment

- Set up your Python package.

We recommend anaconda.

```shell-session
$ pyenv install anaconda3-4.4.0
$ cd [PROJECT_ROOT]
$ pyenv local anaconda3-4.4.0
```

- Set up your `.gitignore`

```shell-session
$ gibo Python > .gitignore
```
